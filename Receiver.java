package pa2;

import	java.util.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import	java.net.*;
import java.nio.ByteBuffer;

public class Receiver {
	
	public static void main(String [] arg) throws Exception
	{
		final byte RED = 0, BLUE = 1, GREEN = 2;
		final byte EOF = 0, QUIT = 1;

		int MSS = 1024;
		DatagramSocket socket;
		DatagramPacket receivePacket = new DatagramPacket(new byte[MSS], MSS),
					   prevAck = makeAck(RED, -1);   //the last-sent ACK, initialized to RED -1
		SocketAddress returnAddress;
		Random rand = new Random();
		int dropRate;     //the user-defined packet drop rate
		int seqNum = 0,   //the expected next seqnum. initialized to 0 because we're awaiting the 0th packet
			seqMax = -1;  //the expected last packet. initialized to -1 b/c we don't know the length of the file yet
		String filename ="";
		ArrayList<byte[]> messages = new ArrayList<>();
				
		if(arg.length != 2){
			System.out.println("Incorrect arguments detected. Default values will be used.");
			socket = new DatagramSocket(3000);
			dropRate = 10;
		}
		else{
			socket = new DatagramSocket(Integer.parseInt(arg[0]));
			dropRate = Integer.parseInt(arg[1]);
		}
		
		socket.setReuseAddress(true);
		
		//report the drop rate
		if(dropRate >= 100){
			System.out.println("Drop rate will be 100%");
		}
		else if(dropRate < 100 && dropRate > 0){
			System.out.println("Drop rate will be " + dropRate + "%");
		}
		else{
			System.out.println("Drop rate will be 0%");
		}
		
		socket.setSoTimeout(0);
		System.out.println("Listening for packets...");
		
		while(true)
		{			
			//receive packet and extract data
			try{
				socket.receive(receivePacket);	
				socket.setSoTimeout(3);
			}
			catch(SocketTimeoutException ste){
				//System.out.println("Timeout: resending last ACK");
				socket.send(prevAck);
				socket.setSoTimeout(0);
				continue;
			}
			
			//choose a random number, if it's higher than the drop rate, handle the packet
			if(rand.nextInt(100) > dropRate){
				//get essential info: packet color and return address
				byte packetColor = receivePacket.getData()[0];
				returnAddress = receivePacket.getSocketAddress();
				prevAck.setSocketAddress(returnAddress);
	
				if(packetColor == GREEN){
					if(receivePacket.getData()[1] == EOF){
						System.out.println("got EOF");
						byte[] eof = new byte[2];
						eof[0] = GREEN;
						eof[1] = EOF;
						
						socket.send(new DatagramPacket(eof, eof.length, returnAddress));
						
						//reset all values for the next file
						seqNum = 0;
						seqMax = -1;
						filename = "";
						prevAck = makeAck(RED, -1);
						prevAck.setSocketAddress(returnAddress);
						socket.setSoTimeout(0);
						messages = new ArrayList<>();
						
						System.out.println("Listening for packets...");
					}
					else if(receivePacket.getData()[1] == QUIT){
						System.out.println("Server terminated, shutting down.");
						byte[] goodbye = new byte[2];
						goodbye[0] = 2;
						goodbye[1] = 1;
						socket.send(new DatagramPacket(goodbye, goodbye.length, returnAddress));
						socket.close();
						return;

					}
				}
				else{
				
					//find the packet's sequence number from bytes 1-4 of receivePacket
					int packetNum = extractSeq(receivePacket.getData(), 1);
				
					//if the packet matches the expected sequence number, handle the packet and send an ACK
					if(packetNum == seqNum){
						//System.out.println("received in-order packet: " + packetNum);
	
						//if packetNum = 0, this packet will contain the file size and name					
						if(packetNum == 0){						
							//the file size (in packets) is contained at bytes 5-9 of packet zero
							seqMax = extractSeq(receivePacket.getData(), 5);
							byte[] filenameBytes = Arrays.copyOfRange(receivePacket.getData(), 9, receivePacket.getLength());
							filename = new String(filenameBytes, 0, filenameBytes.length);
							
							System.out.println("Receiving file: " + filename + " " + ", " + seqMax + " packets long.");
						}
						
						//otherwise, it's a normal in-sequence packet, so extract and store the message data
						else{
							byte message[] = Arrays.copyOfRange(receivePacket.getData(), 5, receivePacket.getLength());
							messages.add(message);
						}
						
						//if incrementing seqNum will cause overflow, change the color
						if(seqNum + 1 < seqNum){
							if(packetColor == RED){
								packetColor = BLUE;
							}
							else{
								packetColor = RED;
							}
						}
						
						//construct and send an ACK with the packet's red/blue byte and sequence number
						DatagramPacket ack = makeAck(packetColor, packetNum);
						ack.setSocketAddress(returnAddress);
						socket.send(ack);
						
						//save the ACK in case of future dropped packets
						prevAck = ack;
						seqNum++;
						
						//if the final packet has been detected, write bytes to file
						if(seqMax > -1 && seqNum >= seqMax && !filename.isEmpty()){
							writeFile(filename, messages);
						}
					}
					
					//if the packet number is higher than expected, we infer dropped packets
					else if(packetNum > seqNum){
						//System.out.println("received out-of-order packet:" + packetNum);
						
						socket.send(prevAck);
					}
				}
			}
			
			//if random number is less than the drop rate, do nothing
			else{
				//System.out.println("dropped a packet: " + extractSeq(receivePacket.getData(), 1));
			}
		}
	}
	
	//this method accepts an array of bytes and an int, and returns the int contained at i - i+4
	public static int extractSeq(byte[] data, int i){
		byte[] seqBytes = new byte[4];
		seqBytes[0] = data[i];
		seqBytes[1] = data[i+1];
		seqBytes[2] = data[i+2];
		seqBytes[3] = data[i+3];
			
		return ByteBuffer.wrap(seqBytes).getInt();		
	}
	
	//this method accepts a filename and an arraylist of byte arrays, and writes the bytes to file
	public static void writeFile(String filename, ArrayList<byte[]> messages) throws IOException{
		System.out.println("Every packet received, writing to file");
		
		File output = new File("recv_" + filename);
		
		FileOutputStream fos = new FileOutputStream(output);
		
		try{
			for(byte[] s: messages){
				fos.write(s);
			}
		}
		finally{
			fos.close();
		}
	}
	
	//this method returns an ACK packet of the given color and seqnum
	public static DatagramPacket makeAck(byte color, int seq){
		byte[] packetBytes = new byte[5];
		packetBytes[0] = color;
		
		byte[] seqBytes = ByteBuffer.allocate(4).putInt(seq).array();
		packetBytes[1] = seqBytes[0];
		packetBytes[2] = seqBytes[1];
		packetBytes[3] = seqBytes[2]; 
		packetBytes[4] = seqBytes[3];
		
		return new DatagramPacket(packetBytes, packetBytes.length);
	}
}