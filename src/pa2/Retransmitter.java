package pa2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import	java.util.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import	java.net.*;

public class Retransmitter implements Runnable{
	
	//Structure for connected clients
	AddrList addrList;
	
	//Connection state storage
	private int status;
	
	//Connection state codes
	final static int 
		UNCONNECTED = 0, 
		CONNECTED = 1, 
		SHAKY = 2;
	
	//Flow control
	final static byte 
		EOF = 0, 
		QUIT = 1;
	
	//Sequencing considerations
	final static byte 
		RED = 0, 
		BLUE = 1, 
		GREEN = 2;
	
	//Inits
	DatagramSocket socket;
	private PacketList packets;
	
	//Constructor
	public Retransmitter(DatagramSocket socket, PacketList packets, int base, AddrList addL){
		this.socket = socket;
		this.packets = packets;
		this.packets.base = base;
		this.addrList = addL;
	}
	
	@Override
	public void run(){
		
		if(addrList == null){
			addrList = new AddrList();
		}
		
		//Buffer
		DatagramPacket ackPacket = new DatagramPacket(new byte[5], 5);//(new byte[5]).length);
	
		//start out with unconnected to give the receiver some time to log on
		status = UNCONNECTED; 
		while(true){
			try{
	
				//If connected (recing acks in small enough amount of time) set timeout to 3 miliseconds
				if(status == CONNECTED){
					socket.setSoTimeout(3);
				}
	
				//Else set the timeout to five seconds
				else if(status == SHAKY){
					socket.setSoTimeout(5000);
				}
	
				//receive the packet
				socket.receive(ackPacket);
				status = CONNECTED;
	
				//If ack is quit msg
				if(ackPacket.getData()[0] == GREEN){
					if(ackPacket.getData()[1] == EOF){
						System.out.println("EOF received, ending thread");
						return;
					}
					else if(ackPacket.getData()[1] == QUIT){
						System.exit(0);
					}
				}
	
	

	
				//Extract the address of the sender:
	
	
				SocketAddress recdAddress = ackPacket.getSocketAddress();
				//If the address is new add it to the addresslist
				//Start a timer for it
				//Retransmit with base = 0
				if(!addrList.seenBefore(recdAddress)){
					addrList.addAddr(0, recdAddress);
					//start a thread for the retransmitter: listens for ACKs and resends packets as needed
					//the sender and the retransmitter have access to PacketList
					Thread rt = new Thread(new Retransmitter(socket, packets, 0, addrList));
					rt.start();

					//start the thread for the transmitter: sends packets
					Thread st = new Thread(new Transmitter(socket, packets));
					st.start();
					return;
				}
	
	
				//extract the sequence number from the packet
				int ackNum = extractSeq(ackPacket.getData(), 1);
				synchronized(packets){
	
					//if the last packet has been acknowledged, send EOF to receiver
					if(ackNum >= packets.seqMax - 1){
						packets.base = ackNum;
						System.out.println("Sending EOF");
						byte[] eof = new byte[2];
						eof[0] = GREEN;
						eof[1] = EOF;
						socket.send(new DatagramPacket(eof, eof.length, packets.destination, packets.port));
						continue;
					}	
	
					//else, we have received an ACK with expected or higher number, which means all prev packets have been received
					else{
						for(int i = ackNum; i >= packets.base; i--){
							packets.removePacket(i);
						}
						
						//System.out.println("received in-order ACK #" + ackNum);
						packets.base = ackNum + 1;
					}
				}
			}
			
			//called if receiver takes too long to respond
			catch(SocketTimeoutException ste){						
				//if status = connected, then socket timed out at 5 miliseconds 
				if(status == CONNECTED){
					status = SHAKY;
					synchronized(packets){
						retransmit();
					}
				}				
			
				//otherwise, if status = shaky, then socket timed out at 5000 miliseconds, and the receiver is probably dead
			
				else if(status == SHAKY){
					System.out.println("Timeout: receiver crashed or shut down.");
					status = UNCONNECTED;
					System.exit(0);
				}
			}
			
			catch (IOException e) {
				e.printStackTrace();
			}
		}
	}	
	
	//this method accepts an array of bytes and an int, and returns the int contained at i - i+4
	public static int extractSeq(byte[] data, int i){
		byte[] seqBytes = new byte[4];
		seqBytes[0] = data[i];
		seqBytes[1] = data[i+1];
		seqBytes[2] = data[i+2];
		seqBytes[3] = data[i+3];
			
		return ByteBuffer.wrap(seqBytes).getInt();		
	}
	
	public void retransmit(){
		//resend all unacknowledged packets, from base til seqNum
		for(int i = packets.base; i < packets.seqNum; i++){
			//System.out.println("\tRetransmitting packet #" + i);
			try {
				socket.send(packets.getPacket(i));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
	}
}