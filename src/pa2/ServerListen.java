package pa2;
import java.util.*;
import java.io.*;
import java.net.*;

public class ServerListen implements Runnable{
	
	private static Server server;
	private static int userCount;
	
	public ServerListen(Server serve, int count){
		server = serve;
		userCount = count;
	}
	
	public void run(){
		try{
			ServerSocket serverSocket = new ServerSocket(3123, 20);
			Socket socket;
			serverSocket.setReuseAddress(true);
			while(true){
				while(server.getAddActivePorts(null).size() < 20){
					socket = serverSocket.accept();
					server.getAddActivePorts(socket);
				}	
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}		
	}
}