package pa2;

import java.io.IOException;
import java.net.DatagramSocket;

public class Transmitter implements Runnable{

	DatagramSocket socket;
	PacketList packets;
	
	public Transmitter(DatagramSocket socket, PacketList packets){
		this.socket = socket;
		this.packets = packets;
	}
	
	public void run() {
		while(true){			
			synchronized(packets){
				//if every packet has been sent, end transmission thread
				if(packets.seqNum == packets.seqMax){
					System.out.println("All packets sent, ending transmission thread");
					return;
				}				
				try{
					//if there's room in the window, send packets	
					if(packets.seqNum < packets.base + packets.size){
						transmit(packets.seqNum);
						//System.out.println("sending: seqnum = " + packets.seqNum + " base = " + packets.base);
						packets.seqNum++;
					}
				}
				catch(IOException e){
					// TODO Auto-generated catch block
					e.printStackTrace();
				}	
			}
		}
	}
	
	//sends packet i, as long as the retransmit thread is not retransmitting
	public void transmit(int i) throws IOException{
		socket.send(packets.getPacket(i));			
	}
}
