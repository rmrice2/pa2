package pa2;

import java.io.*;
import java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;


public class SendFile implements Runnable{ 

	DatagramSocket dest;
 	File toSend; 
	
	public SendFile(DatagramSocket destin, File send){
		dest = destin;
		toSend = send;
	}
	
	public void run() {
		try{
			InetAddress destination = InetAddress.getByName( "128.6.13.255" );
			//InetAddress destination = InetAddress.getByName(dest);
			DatagramSocket socket = dest;
			int	port = 3000;
			int windowSize = 10;
			//System.out.println( "datagram target is " + destination + " port " + port );
    		
			Path path = toSend.toPath();
			byte[] data = Files.readAllBytes(path);
			
			//create the window for holding outgoing packets
			//on construction, PacketList will create enough packets to fill the window
			PacketList packets = new PacketList(toSend.getName(), data, windowSize, destination, port);
			
			//start a thread for the retransmitter: listens for ACKs and resends packets as needed
			//the sender and the retransmitter have access to PacketList
			Thread rt = new Thread(new Retransmitter(socket, packets));
			rt.start();
    		
			//start the thread for the transmitter: sends packets
			Thread st = new Thread(new Transmitter(socket, packets));
			st.start();
    		
			//wait until both threads return
			st.join();
			rt.join();
			
			System.out.print( "File transmitted.");
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}