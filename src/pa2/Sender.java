package pa2;

import	java.io.*;
import	java.net.*;
import java.nio.file.Files;
import java.nio.file.Path;

class Sender {

	public static void main(String [] arg) throws Exception{
		InetAddress destination = InetAddress.getByName("Music.local");
		
		//InetAddress destination = InetAddress.getByName( arg[0] );
		DatagramSocket socket = new DatagramSocket();
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		int	port = 3000;
		int windowSize = 10;
		
		System.out.println("datagram target is " + destination + " port " + port);
		System.out.print("Enter a string (type \'exit\' to quit)>>");
		
		String s;
		while((s = stdIn.readLine()) != null){
			//if the user types exit, send a goodbye message to the receiver and sign off
			if(s.equalsIgnoreCase("exit")){
				byte[] goodbye = new byte[2];
				goodbye[0] = (byte)2;  //make sure the first byte of the message is neither red nor blue
				goodbye[1] = (byte)1;
				socket.setSoTimeout(5000);
				socket.send(new DatagramPacket(goodbye, goodbye.length, destination, port));
				//wait for an ACK
				while(true){
					try{
						DatagramPacket p = new DatagramPacket(goodbye, goodbye.length);
						socket.receive(p);
						if(p.getData()[0] == 2 && p.getData()[1] == 1){
							System.out.println("Receiver terminated, ending program");
							socket.close();
							return;
						}
						else{
							socket.send(new DatagramPacket(goodbye, goodbye.length, destination, port));
						}
					}
					catch(SocketTimeoutException ste){
						System.out.println("Receiver not responding, ending program");
						socket.close();
						return;
					}
				}
			}
			//check the filename. if it's invalid, skip the rest of the loop
			File file = new File(s);
			if(!file.exists()){
				System.out.println( "File does not exist.");
				System.out.print("Enter a string (type \'exit\' to quit)>>");
				continue;
			}
			Path path = file.toPath();
			byte[] data = Files.readAllBytes(path);
			
			//create the window for holding outgoing packets
			//on construction, PacketList will create enough packets to fill the window
			PacketList packets = new PacketList(s, data, windowSize, destination, port);
			
			//start a thread for the retransmitter: listens for ACKs and resends packets as needed
			//the sender and the retransmitter have access to PacketList
			Thread rt = new Thread(new Retransmitter(socket, packets, 0, null));
			rt.start();

			//start the thread for the transmitter: sends packets
			Thread st = new Thread(new Transmitter(socket, packets));
			st.start();

			//wait until both threads return
			st.join();
			rt.join();
			
			System.out.print( "File transmitted.\nEnter a string (type \'exit\' to quit)>>" );
		}
		System.out.println( "Normal end of sender." );
		socket.close();
	}
}
