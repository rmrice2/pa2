package pa2;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import	java.util.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import	java.net.*;

public class AddrList{
	
	private static class User{
		int lastAckNum;
		SocketAddress addr;
		public User(int ack, SocketAddress add){
			lastAckNum = ack;
			addr = add;
		}
	}
	
	private static ArrayList<User> addrs = new ArrayList<User>();
	public static SocketAddress lastNew = null;
	public static void addAddr(int lastAck, SocketAddress addr){
		addrs.add(new User(lastAck, addr));
		lastNew = addr;
	}
	
	public static void remAddr(SocketAddress addr){
		for(int i = 0; i < addrs.size(); i++){
			if(addrs.get(i).addr.equals(addr)){
				addrs.remove(i);
				return;
			}
		}
	}
	
	public static boolean seenBefore(SocketAddress addr){
		for(int i = 0; i < addrs.size(); i++){
			if(addrs.get(i).addr.equals(addr)){
				return true;
			}
		}
		return false;
	}
	
}