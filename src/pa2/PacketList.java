package pa2;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.HashMap;

public class PacketList {
	
	static final int RED = 0, BLUE = 1;
	static final int DATA_SIZE = 1019;
	
	int 
		//the highest unacknowledged packet 
		base,    
	    //the highest unsent packet
		seqNum,  
	    //the number of total packets - (bytes in file) / 507
		seqMax,  
	    //the number of packets allowed in the hash map at any one time
		size;    
	
	private HashMap<Integer, DatagramPacket> packets;
	private DatagramPacket[] data;
	private byte[] fileBytes;      //the array of bytes from the original file
	private int packetsMade;  //the number of packets made so far
	private byte currentColor = RED;
	private int currentSeq = 1;
	InetAddress destination;
	int port;

	public PacketList(String filename, byte[] fileBytes, int size, InetAddress destination, int port){
		packets = new HashMap<>();
		base = 0;
		seqNum = 0;
		this.fileBytes = fileBytes;
		this.size = size;
		this.destination = destination;
		this.port = port;
		seqMax = (fileBytes.length / DATA_SIZE) + 2;
		System.out.println("Filesize is " + fileBytes.length + " bytes");
		data = new DatagramPacket[seqMax];
		//make the first packet: RED, 0, FILESIZE, FILENAME
		byte[] welcomePacket = makeWelcomePacket(filename, seqMax);
		data[0] = new DatagramPacket(welcomePacket, welcomePacket.length, destination, port);
		//make the normal data packets
		for(int i = 1; i < seqMax - 1; i++){
			data[i] = makePacket(i);
			//System.out.println("Packet #" + i + " = " + currentColor + " " + currentSeq);
			if(currentSeq + 1 < currentSeq){
				currentSeq = 0;
				if(currentColor == RED){
					currentColor = BLUE;
				}
				else{
					currentColor = RED;
				}
			}
			else{
				currentSeq++;
			}
		}

		//make the final packet with the remainder of the file data
		data[seqMax - 1] = makeRemainderPacket(seqMax - 1);
		
		//fill up the window with packets
		for(int i = 0; i < size && i < seqMax; i++){
			putPacket(i, data[i]);
		}
		packetsMade = size;
		
	}
	
	public DatagramPacket getPacket(int i){
		return packets.get(i);
	}
	
	public boolean putPacket(int i, DatagramPacket p){
		//if there's not room in the window, return false
		if(packets.size() >= size || packetsMade == seqMax){
			return false;
		}
		//otherwise, add the packet and return true
		packets.put(i, p);
		return true;
	}

	//removes the indicated packet and adds a new one, as long as there are packets to add
	public void removePacket(int i){
		if(packets.containsKey(i)){
			packets.remove(i);
		}
		
		if(packetsMade < seqMax){
			putPacket(packetsMade, data[packetsMade]);
			packetsMade++;
		}
	}
	
	//returns a DatagramPacket with seq = i
	public DatagramPacket makePacket(int i){
				
		byte[] packetData = new byte[1024];
		
		//zeroth byte reserved for red/blue info
		packetData[0] = currentColor;
		
		//1st - 4th bytes reserved for seqnum integer info
		byte[] seqBytes = ByteBuffer.allocate(4).putInt(currentSeq).array();
		packetData[1] = seqBytes[0];
		packetData[2] = seqBytes[1];
		packetData[3] = seqBytes[2]; 
		packetData[4] = seqBytes[3];
		
		int dataLoc = i - 1;
		//5th - 512th bytes reserved for data
		for(int j = dataLoc * DATA_SIZE, k = 5; k < 1024 && j < fileBytes.length; j++, k++){
			packetData[k] = fileBytes[j];
		}
		
		//bundle and address the packet
		return new DatagramPacket(packetData, packetData.length, destination, port);
	}
	//make the first packet: RED, 0, FILESIZE, FILENAME
	private static byte[] makeWelcomePacket(String filename, int seqMax){
		byte[] welcomePacket = new byte[1 + 4 + 4 + filename.getBytes().length];
		welcomePacket[0] = (byte) RED;
		byte[] zeroBytes = ByteBuffer.allocate(4).putInt(0).array();
		for(int i = 1, j = 0; j < zeroBytes.length; i++, j++){
			welcomePacket[i] = zeroBytes[i-1];
		}
		byte[] filesizeBytes = ByteBuffer.allocate(4).putInt(seqMax).array();
		for(int i = 5, j = 0; j < filesizeBytes.length; i++, j++){
			welcomePacket[i] = filesizeBytes[j];
		}
		byte[] filenameBytes = filename.getBytes();
		for(int i = 9, j = 0; i < welcomePacket.length; i++, j++){
			welcomePacket[i] = filenameBytes[j];
		}
		return welcomePacket;
	}
	public DatagramPacket makeRemainderPacket(int i){
		//make the final packet with the remainder bytes
		int remainder = fileBytes.length % DATA_SIZE;
		byte[] remainderPacket = new byte[remainder+5];
		//zeroth byte reserved for red/blue info
		remainderPacket[0] = 0;
		//1st - 4th bytes reserved for seqnum integer info
		byte[] seqBytes = ByteBuffer.allocate(4).putInt(i).array();
		remainderPacket[1] = seqBytes[0];
		remainderPacket[2] = seqBytes[1];
		remainderPacket[3] = seqBytes[2]; 
		remainderPacket[4] = seqBytes[3];
		for(int k = 5, j = (seqMax - 2) * DATA_SIZE; k < remainder + 5; k++, j++){
			remainderPacket[k] = fileBytes[j];
		}
		return new DatagramPacket(remainderPacket, remainderPacket.length, destination, port);		
	}
}
