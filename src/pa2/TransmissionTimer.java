package pa2;
import java.util.Timer;
import java.util.TimerTask;

public class TransmissionTimer extends Thread{
	//0 seconds -> 5 ms
	public int seconds;
	
	public TransmissionTimer(int secs){
		seconds = secs;
	}
	
	public void run(){
		try {
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	/**
	//Make a timer for the given IP
	//On timeout from any timer 
	//Create a new retransmitter with base of previous ack for the timer that went off
	
	private static TimerTask timerTask = new TimerThread();
	
	public void run(){

		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(timerTask, 0, 10 * 1000);
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		timer.cancel();
	}
	
	private static class TimerThread extends TimerTask {
		@Override
		public void run() {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
		**/
}